const { resolve } = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'production',

    entry: {
        styles: './scss/main.scss',
        validation: './js/form-validation.js'
    },

    output: {
        filename: '[name].js',
        path: resolve(__dirname, './dist'),
        publicPath: '/',
    },

    target: 'web',

    context: resolve(__dirname, './src'),

    resolve: {
        extensions: ['.js','.scss'],
        alias: {
            bemKit: 'bem-kit/src/_scss'
        }
    },

    externals: {
        jquery: 'jQuery'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            query: {
                                sourceMap: true,
                            },
                        },
                    ],
                    publicPath: '../'
                })),
            },
        ]
    },

    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextPlugin({
            filename: 'styles.css',
            disable: false,
            allChunks: true
        }),
        new UglifyJsPlugin({
            test: /\.js($|\?)/i,
            exclude: /\/node_modules/,
            uglifyOptions: {
                output: {
                    comments: false,
                    beautify: false
                },
                ie8: false,
                safari10: false,
            }
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template:  resolve(__dirname,'./src/form.html'),
            minify: {
                collapseWhitespace: true
            }
        })
    ],
};