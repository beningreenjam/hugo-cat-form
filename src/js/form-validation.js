import $ from 'jquery';

$(document).ready(() => {
    $('.textfield').on('focusout', (event) => {
        let el = $(event.currentTarget),
            elParent = el.closest('.field-wrapper');

        if(el.is('.input-validation-error')) {
            elParent.addClass('field-wrapper--error');
        }
        else {
            elParent.removeClass('field-wrapper--error');
        }
    });
});