#Hugo & Cat Form Test

This was a simple test of styling a ready made form HTML page, following the BEM methodology and adding validation.

[Demo](https://unobtrusive-validation.netlify.com)

##Development Instructions

`npm install && npm start`