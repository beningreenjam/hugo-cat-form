const { resolve } = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'development',

    devtool: 'inline-source-map',

    stats: "minimal",

    entry: {
        styles: './scss/main.scss',
        validation: './js/form-validation.js'
    },

    output: {
        filename: '[name].js',
        path: resolve(__dirname, './dist'),
        publicPath: '',
    },

    context: resolve(__dirname, './src'),

    resolve: {
        extensions: ['.js','.scss'],
        alias: {
            bemKit: 'bem-kit/src/_scss'
        }
    },

    externals: {
        jquery: 'jQuery'
    },

    devServer: {
        open: true,
        host: 'localhost',
        port: 3000,
        hot: true,
        noInfo: true,
        contentBase: resolve(__dirname, './dist'),
        historyApiFallback: true,
        publicPath: ''
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: require.resolve('jquery'),
                loader: 'expose-loader?$!expose-loader?jQuery'
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            query: {
                                sourceMap: true,
                            },
                        },
                    ],
                    publicPath: '../'
                })),
            }
        ]
    },

    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery'
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
        }),
        new ExtractTextPlugin({
            filename: 'styles.css',
            disable: false,
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template:  resolve(__dirname,'./src/form.html'),
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
};